<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="resources/bootstrap/css/bootstrap.min.css">
    <script src="resources/bootstrap/js/bootstrap.min.js"></script>
    <style>
        body{
            background-color: #2b669a;
        }
        .myList{
            list-style-type: none;
            display: inline-block;
        }
    </style>
    <title>Project Home</title>
</head>
<body>
<h1 style="font-weight: bold; font-family: Algerian; text-align: center">Welcome to Atomic Project Dashboard</h1>
<div class="container">
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <br>
        <br>
        <br>
        <br>
        <div class="panel panel-default">
                <div class="panel-heading" style="text-align: center; font-weight: bold">Navigation Panel</div>
                <div class="panel-body">
                    <div class="col-md-6">
                        <br>
                        <br>
                        <li class="myList">
                            <ul><a href="views/SEID164311/City/index.php" target="_blank" class="btn btn-success" role="button"><span class=" glyphicon glyphicon-user"> City</a></ul>
                            <ul><a href="views/SEID164311/Gender/index.php" target="_blank" class="btn btn-success" role="button"> <span class=" glyphicon glyphicon-user"> Gender</a></ul>
                            <ul><a href="views/SEID164311/BirthDay/index.php" target="_blank" class="btn btn-success" role="button"><span class=" glyphicon glyphicon-user"> Birthday</a></ul>
                            <ul><a href="views/SEID164311/Hobbies/index.php" target="_blank" class="btn btn-success" role="button"><span class=" glyphicon glyphicon-user"> Hobbies</a></ul>



                        </li>

                    </div>

                    <div class="col-md-6">
                        <br>
                        <br>
                        <ul><a href="views/SEID164311/BookTitle/index.php" target="_blank" class="btn btn-success" role="button"><span class="glyphicon glyphicon-user">  Book Title</a></ul>
                        <ul><a href="views/SEID164311/ProfilePicture/index.php" target="_blank" class="btn btn-success" role="button"> <span class=" glyphicon glyphicon-user"> Profile Picture</a></ul>
                        <ul><a href="views/SEID164311/OrganaizationSummary/index.php" target="_blank" role="button" class="btn btn-success"> <span class=" glyphicon glyphicon-user"> Organization Summary</a></ul>
                        <br>
                    </div>
                    <br>
                    <br>
                    <br>
                    <h3 style="text-align: center">Team - Error Squad </h3>
                </div>
            </div></div>
    <div class="col-md-2"></div>

    </div>

</div>
</div>


</body>
</html>