<?php

require_once("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();

use App\Message\Message;

$msg = Message::message();

echo "<div>  <div id='message'>  $msg </div>   </div>";

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <title>Organization Summary</title>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <h2 style="text-align: center; color: #1b6d85">Organization Summary</h2>
            <form class="form-horizontal" action="store.php"; method="post">
                <div class="form-group">

                    <label for="booktitle" >Enter Your Name</label>
                    <input type="text" class="form-control" name="name" <?php
                    if(empty($_POST))
                    {
                        echo "You did not fill out the required fields.";
                    }

                    ?>>
                </div>

<!--                <br>-->
                <div class="form-group">
                    <label for="organizationname" >Enter Organization Name</label>
                    <input type="text" class="form-control" name="organizationName" <?php
                    if(empty($_POST))
                    {
                        echo "You did not fill out the required fields.";
                    }

                    ?>>
                </div>
<!--                <br>-->
                <div class="form-group">
                    <label for="texbox">Enter Organization Summary</label>
                    <br>
                    <textarea name="txtsummary" id="summary" cols="85" rows="10"></textarea>
                    
                </div>

                <input class="btn btn-success" type="submit" value="submit" <?php
                if(empty($_POST))
                {
                    echo "You did not fill out the required fields.";
                }

                ?>>
                <a href='index.php' class='btn btn-primary' role='button'>  <span class='glyphicon glyphicon-home'> Return Home</a>
        </div>

    </div>
    <div class="col-md-3"></div>
</div>


<script src="../../../resources/bootstrap/js/jquery.js"></script>

<script>

    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>


</body>
</html>