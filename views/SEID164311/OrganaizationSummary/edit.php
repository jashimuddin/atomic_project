<?php

require_once("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();

use App\Message\Message;
$msg = Message::message();

$obj = new App\OrganizationSummary\OrganizationSummary();
$obj->setData($_GET);
$singleData = $obj->view();


echo "<div>  <div id='message'>  $msg </div>   </div>";

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <title>Organization Summary</title>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <h2 style="text-align: center; color: #1b6d85">Organization Summary</h2>
            <form class="form-horizontal" action="update.php" method="post">
                <div class="form-group">

                    <label for="name" >Enter Your Name</label>
                    <input type="text" class="form-control" name="name" value="<?php echo $singleData->user_name ?>" >
                </div>

                <!--                <br>-->
                <div class="form-group">
                    <label for="organizationname" >Enter Organization Name</label>
                    <input type="text" class="form-control" name="organizationName" value="<?php echo $singleData->organization_name ?>">
                </div>
                <!--                <br>-->
                <div class="form-group">
                    <label for="texbox">Enter Organization Summary</label>
                    <br>


                    <!--textarea name="txtsummary" id="summary" cols="85" rows="10"> </textarea>-->


                    <?php

                    echo "<textarea name='txtsummary' id='name' cols='85' rows='10' >\n" . htmlspecialchars($singleData->summary) ." " ."</textarea>" ;

                    ?>

                    <!-- Send id with hidden mode to update associated id -->
                    <input type="hidden" name="id" value="<?php echo $singleData->id ?>">
                    <!-- Send id with hidden mode to update associated id -->

                </div>

                <input class="btn btn-success" type="submit" value="submit">
        </div>

    </div>
    <div class="col-md-3"></div>
</div>


<script src="../../../resources/bootstrap/js/jquery.js"></script>

<script>

    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>


</body>
</html>