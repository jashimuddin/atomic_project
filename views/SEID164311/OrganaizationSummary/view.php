<?php
require_once ("../../../vendor/autoload.php");

$obj = new \App\OrganizationSummary\OrganizationSummary();

$obj->setData($_GET);

$singleData = $obj->view();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <title>Document</title>
</head>
<body>
<h1>Single Organization's Summary  Information</h1>
<!------------  Nav bar started -------------------------->

<div class="nav navbar">
    <a href="../../../navigation.php" class="btn btn-primary" role="button">Home Page</a>
    <a href="index.php" class="btn btn-primary" role="button"> << </a>

</div>




<!--------------- Nav bar started -------------------->

<table class="table table-bordered table-striped">
    <?php
        echo "
        
        <tr><td>ID:</td><td>$singleData->id</td></tr>
        <tr><td>User Name:</td><td>$singleData->user_name</td></tr>
        <tr><td>Organization Name:</td><td>$singleData->organization_name</td></tr>
        <tr><td>Summary:</td><td>$singleData->summary</td></tr>
    
        
        "

    ?>
</table>


</body>
</html>
