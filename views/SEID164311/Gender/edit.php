<?php

require_once("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();

use App\Message\Message;
use App\Gender\Gender;
use App\Utility\Utility;


if(!isset($_GET['id'])) {

    Message::message("You can't visit view.php without id (ex: view.php?id=23)");
    Utility::redirect("index.php");
}

$obj = new Gender();
$obj->setData($_GET);
$singleData = $obj->view();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <title>Gender</title>
    <style>
        body{
            background-color: #1b6d85;
        }
    </style>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <h2 style="text-align: center; color: deeppink">Gender Information</h2>
            <form class="form-group" action="update.php" method="post">
                <label>Name</label>
                <input class="form-control col-sm-10"  type="text" name="name" value="<?php echo $singleData->name ?>">
                <br>
                <br>
                <br>
                <br>
                <label>Gender:</label>
                <input type="radio" name="gender" value = "Male" <?php if($singleData->gender == "Male") echo 'checked="checked"' ?>> Male
                <input type="radio" name="gender" value = "Female" <?php if($singleData->gender == "Female") echo 'checked="checked"' ?>"> Female


                <br>
                <!-- Send id with hidden mode to update associated id -->
                <input type="hidden"  class="form-control" name="id" value="<?php echo $singleData->id ?>">
                <!-- Send id with hidden mode to update associated id -->

                <input class="btn btn-success" type="submit" value="Update">

            </form>

        </div>

    </div>
    <div class="col-md-3"></div>
</div>

<script src="../../../resources/bootstrap/js/jquery.js"></script>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>


</body>
</html>