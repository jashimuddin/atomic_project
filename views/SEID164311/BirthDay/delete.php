<?php
require_once ("../../../vendor/autoload.php");

use App\BirthDay\BirthDay;
use App\Utility\Utility;

$obj = new BirthDay();
$obj->setData($_GET);

$obj->delete();

$path = $_SERVER['HTTP_REFERER'];

Utility::redirect($path);



