<?php
require_once ("../../../vendor/autoload.php");
use App\Utility\Utility;
use App\Message\Message;
$obj = new App\ProfilePicture\ProfilePicture;

$allData = $obj->trashed();
$msg = Message::message();


?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>

    <title>Trshed List</title>
</head>
<body>

<?php echo "<div>  <div align='center' class=' alert-info ' id='message'>  $msg </div>   </div>";   ?>


<h1 style="color: #000; text-align: center">Trashed Profile List List</h1>

<form id="selectionForm" action="recover_multiple.php"  method="post">

    <div class="nav navbar">
        <a href="index.php" class="btn btn-primary" role="button">Home</a>
        <input  class="btn btn-success" type="submit" value="Recover Multiple" >
        <input  class="btn btn-danger" type="button" id="deleteMultiple" value="Delete Multiple" >
        <a href="index.php" class="btn btn-primary" role="button"> << </a>

    </div>

<table class="table table-bordered table-striped">

    <tr>
        <th>Select All <input type="checkbox" id="select_all" ></th>
        <th>Serial</th>
        <th>ID</th>
        <th>Name</th>
        <th>Profile Picture</th>
        <th>Action Button</th>
    </tr>

    <?php
    $serial = 1;
    foreach ($allData as $row){
        echo "
             <tr>
                 <td><input  type='checkbox' class='checkbox' name='selectedIDs[]' value='$row->id'></td>
                 <td>$serial</td>
                 <td>$row->id</td>
                 <td>$row->name</td>
                 <td><img src='Images/$row->profile_picture' height='100px' width='100px'></td>
                 
                 <td>  
                    <a href='view.php?id=$row->id' class='btn bg-primary' role='button'><span class='glyphicon glyphicon-eye-open'> View</a>     
                    <a href='edit.php?id=$row->id' class='btn btn-warning' role='button'> <span class='glyphicon glyphicon-edit'> Edit </a>     
                    <a href='recover.php?id=$row->id' class='btn btn-success' data-toggle='confirmation' role='button'> <span class='glyphicon glyphicon-edit'> Recover</a>     
                    <a href='delete.php?id=$row->id' class='btn btn-danger' onclick='return confirm_delete()' role='button'><span class='glyphicon glyphicon-warning-sign'>  Delete</a>
                 </td>
                 
                 
             </tr>
            ";
        $serial++;
    }
    ?>


</table>

</form>
<!-- Java script for message -->

<script src="../../../resources/bootstrap/js/jquery.js"></script>

<script>
    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>

<script>
    function confirm_delete(){
        return confirm(" Are you sure? ");
    }
</script>


<script>

    //select all checkboxes
    $("#select_all").change(function(){  //"select all" change
        var status = this.checked; // "select all" checked status
        $('.checkbox').each(function(){ //iterate all listed checkbox items
            this.checked = status; //change ".checkbox" checked status
        });
    });

    $('.checkbox').change(function(){ //".checkbox" change
//uncheck "select all", if one of the listed checkbox item is unchecked
        if(this.checked == false){ //if this item is unchecked
            $("#select_all")[0].checked = false; //change "select all" checked status to false
        }

//check "select all" if all checkbox items are checked
        if ($('.checkbox:checked').length == $('.checkbox').length ){
            $("#select_all")[0].checked = true; //change "select all" checked status to true
        }
    });

</script>



<script>

    $("#deleteMultiple").click(function(){
        var r = confirm("Are you sure you want to delete selected record(s)");

        if(r==true){

            var selectionForm = $("#selectionForm");
            selectionForm.attr("action","delete_multiple.php");
            selectionForm.submit();
        }
    });

</script>



<!--------------------------------  End of javascript      -------------------->


</body>
</html>
