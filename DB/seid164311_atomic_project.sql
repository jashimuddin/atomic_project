-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 11, 2017 at 05:55 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `seid164311_atomic_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE `birthday` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `birthdate` date NOT NULL,
  `is_trashed` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `birthdate`, `is_trashed`) VALUES
(18, 'Mamun Ur Rashid khan', '0000-00-00', '2017-06-10 21:20:50'),
(19, 'Salauddin ', '2017-06-08', '2017-06-10 21:20:50'),
(21, 'Jashim Uddin', '2017-06-15', 'No'),
(23, 'sasd', '0000-00-00', '2017-06-10 21:22:45'),
(24, 'Jashim Uddin', '2017-06-28', '2017-06-10 21:22:45'),
(25, 'Shahana Chowdhuary', '2017-06-15', '2017-06-10 21:26:33'),
(26, 'wWEW', '2017-06-23', 'No'),
(27, 'Jashim Uddin', '2017-06-09', 'No'),
(28, 'Sahid Ul Islam', '2017-01-02', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(11) NOT NULL,
  `book_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `author_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_title`, `author_name`, `is_trashed`) VALUES
(65, 'Visual Basic 6.0', 'Mahbubur Rahman', 'No'),
(66, 'Adobe Photoshop', 'Mahbubur Rahman', 'No'),
(67, 'Visual Basic 6.0', 'Shureed Sarkar', 'No'),
(68, 'Visual Basic 6.0', 'Besley & Brighham', 'No'),
(71, 'adobe photoshop', 'Besley & Brighham', 'No'),
(72, 'Essentials of Manageraial Finance 13E', 'Besley & Brighham', 'No'),
(73, 'Thesaurus', 'Oxford', 'No'),
(74, 'Applied Mathmatics', 'Ann J.Hughes', 'No'),
(75, 'Micro Economics', 'Kutsoyanis', 'No'),
(76, 'Business Mathmatics', 'Edward Robinson', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(100) NOT NULL,
  `name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `city`, `is_trashed`) VALUES
(1, 'Jashim Uddin', 'Barisal', 'No'),
(2, 'Jashim Uddin', 'Chittagong', 'No'),
(3, 'Shahana Chowdhuary', 'Dhaka', 'No'),
(4, 'Sahid Ul Islam', 'Sylhet', 'No'),
(6, 'Sahid Ul Islam', 'Rajshahi', 'No'),
(7, 'reaeer', 'Khulna', 'No'),
(8, 'eqezc', 'Rangpur', 'No'),
(9, 'ada', 'Barisal', 'No'),
(10, 'Sdehh', 'Rangpur', 'No'),
(11, 'add', 'Dhaka', 'No'),
(12, 'Shahana Chowdhuary', 'Chittagong', 'No'),
(13, 'Shahana Chowdhuary', 'Chittagong', 'No'),
(14, 'Shahana Chowdhuary', 'Dhaka', 'No'),
(15, 'Eadredt', 'Barisal', 'No'),
(16, '', 'Dhaka', 'No'),
(17, 'Jashim Uddin', 'Barisal', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `gender`, `is_trashed`) VALUES
(25, 'Shahana', 'Female', 'No'),
(26, 'Fariha khanom', 'Female', '2017-06-11 12:18:29'),
(27, 'Boltu', 'Male', 'No'),
(28, 'Motu', 'Male', 'No'),
(29, 'sakira', 'Female', 'No'),
(30, 'wwchane', 'Male', 'No'),
(31, 'TshuiMoui', 'Female', 'No'),
(32, 'Babor Al Masud', 'Male', 'No'),
(33, 'Arman', 'Male', 'No'),
(34, 'McGlene', 'Male', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `hobbies` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `name`, `hobbies`, `is_trashed`) VALUES
(6, 'Jashim Uddin', 'Gardening, Bycycling', '2017-06-10 21:21:32'),
(11, 'Shahana Chowdhuary', 'Gardening, Travelling', 'No'),
(14, 'wWEW', 'Gardening, Travelling, Bycycling', 'No'),
(15, 'sasd', 'Gardening, Collecting Books, Gaming', 'No'),
(16, 'Adidas', 'Gardening, Collecting Books, Travelling, Bycycling, Gaming', 'No'),
(17, 'koilas', 'Gardening, Gaming', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `organization_summaries`
--

CREATE TABLE `organization_summaries` (
  `id` int(11) NOT NULL,
  `user_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `organization_name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `summary` varchar(1500) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `organization_summaries`
--

INSERT INTO `organization_summaries` (`id`, `user_name`, `organization_name`, `summary`, `is_trashed`) VALUES
(9, 'Jashim Uddin', 'Error squad', 'We do errors so we does', 'No'),
(10, 'Atema', 'The A Team', 'best team', 'No'),
(11, 'MacCinon', 'Adaew', 'a quick brownn fox jumps over the lazy dog \r\na quick brownn fox jumps over the lazy dog ', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `profile_pictures`
--

CREATE TABLE `profile_pictures` (
  `id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `profile_picture` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `profile_pictures`
--

INSERT INTO `profile_pictures` (`id`, `name`, `profile_picture`, `is_trashed`) VALUES
(11, 'Jashim Uddin', '1496871351ncnharry.jpg', 'No'),
(15, 't5554', '14970211523.JPG', 'No'),
(16, 'tj mhku', '14970211594.JPG', 'No'),
(20, 'u633', '1497021180111.JPG', 'No'),
(21, 'jyurtr', '149716267710.png', 'No'),
(22, 'werwe', '1497021190FILE510.JPG', 'No'),
(23, 'r4343', '1497162670birth.jpg', 'No'),
(24, 'Jashim Uddin', '1497163819book.png', 'No'),
(25, 'Sahid Ul Islam', '1497163823', 'No'),
(26, 'Shahana Chowdhuary', '1497163828email.jpg', 'No');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organization_summaries`
--
ALTER TABLE `organization_summaries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_pictures`
--
ALTER TABLE `profile_pictures`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `organization_summaries`
--
ALTER TABLE `organization_summaries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `profile_pictures`
--
ALTER TABLE `profile_pictures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
