<?php


namespace App\OrganizationSummary;
use PDO;
use App\Message\Message;
use App\Model\Database;
use App\Utility\Utility;


class OrganizationSummary extends Database
{
    public $id;
    public $name;
    public $organizationName;
    public $txtsummary;


    public function setData($postArray){

        if (array_key_exists("id", $postArray)){
            $this->id = $postArray['id'];
        }
        if (array_key_exists("name", $postArray)){
            $this->name = $postArray['name'];
        }
        if (array_key_exists("organizationName", $postArray)){
            $this->organizationName = $postArray['organizationName'];
        }

        if (array_key_exists("txtsummary", $postArray)){
            $this->txtsummary = $postArray['txtsummary'];
        }

    } //end of set data


    public function store(){

            $name = $this->name;
            $organizationName = $this->organizationName;
            $txtsummary = $this->txtsummary;

            $sqlQuery = "INSERT INTO `organization_summaries` (`user_name`, `organization_name`, `summary`) VALUES (?,?,?);";

            $dataArray = array($name, $organizationName, $txtsummary);

            $sth = $this->dbh->prepare($sqlQuery);

            $result = $sth->execute($dataArray);

//          Utility::dd($result);

            if ($result){
                Message::message("Success! Data has been inserted successfully");
            }else{
                Message::message("Failure! Data has not been inserted due to error");
            }

    } // end of store method



    public function index(){
        $sqlQuerry = "select*from organization_summaries WHERE is_trashed = 'No' ";

        $sth = $this->dbh->query($sqlQuerry);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        return $sth->fetchAll();
    } // end of index method

    public function view(){

        $sqlQuerry = "select * from organization_summaries WHERE id=" .$this->id;
        $sth = $this->dbh->query($sqlQuerry);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        return $sth->fetch();
    }


    public function update(){


        $sqlQuerry = "UPDATE organization_summaries SET user_name = ?, organization_name = ?, summary= ? WHERE id = $this->id;";

        $dataArray = array($this->name, $this->organizationName, $this->txtsummary);

        $sth = $this->dbh->prepare($sqlQuerry);

        $result = $sth->execute($dataArray);

        if ($result){
            Message::message("Success! Data has been updated successfully");
        }else{
            Message::message("Failure! Data has not been updated due to error");
        }

    }  // end of update method


    public function trash(){

        $sqlQuery = "update organization_summaries SET is_trashed = NOW() WHERE id = $this->id; " ;


     $result =   $this->dbh->exec($sqlQuery);

        if ($result){
            Message::message("Success! Data has been trashed successfully");
        }else{
            Message::message("Failure! Data has not been trashed due to error");
        }
    } // end of trash method



    public function trashed(){
        $sqlQuery = "select * from organization_summaries WHERE is_trashed <> 'No' ";

        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        return $sth->fetchAll();


    } // end of trashed method

public function recover(){
        $sqlQuery = "update organization_summaries SET is_trashed = 'No' WHERE id= $this->id; " ;

    $result = $this->dbh->exec($sqlQuery);

    if($result){
        Message::message("Success! Data has been recovered successfully!");
    }
    else{
        Message::message("Failure! Data has not been recovered due to an error!");
    }

}  // end of recover method

    public function delete(){

    $sqlQuery = "DELETE FROM organization_summaries WHERE id = $this->id";

    $result = $this->dbh->exec($sqlQuery);
        if ($result){
            Message::message("Success! Data has been deleted successfully");
        }else{
            Message::message("Failure! Data has not been deleted due to error");
        }

    } // end of delete method



    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byName']) && isset($requestArray['byOrganization']) )  $sql = "SELECT * FROM `organization_summaries` WHERE `is_trashed` ='No' AND (`user_name` LIKE '%".$requestArray['search']."%' OR `organization_name` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byName']) && !isset($requestArray['byOrganization']) ) $sql = "SELECT * FROM `organization_summaries` WHERE `is_trashed` ='No' AND `user_name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byName']) && isset($requestArray['byOrganization']) )  $sql = "SELECT * FROM `organization_summaries` WHERE `is_trashed` ='No' AND `organization_name` LIKE '%".$requestArray['search']."%'";

        $sth  = $this->dbh->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $someData = $sth->fetchAll();

        return $someData;

    }// end of search()




    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->organization_name);
        }

        $allData = $this->index();


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->organization_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->user_name);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->user_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords


    public function indexPaginator($page=1,$itemsPerPage=3){


        $start = (($page-1) * $itemsPerPage);

        if($start<0) $start = 0;


        $sqlQuery = "SELECT * from organization_summaries  WHERE is_trashed = 'No' LIMIT $start,$itemsPerPage";


        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $someData  = $sth->fetchAll();

        return $someData;


    } // end of index paginator method


} //end of organization summary