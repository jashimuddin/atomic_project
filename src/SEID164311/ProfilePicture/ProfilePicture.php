<?php


namespace App\ProfilePicture;


use App\Message\Message;
use App\Model\Database;
use App\Utility\Utility;
use PDO;
class ProfilePicture extends Database {


    public $id;
    public $name;
    public $profilePicture;


    public function setData($dataArray){

        if (array_key_exists("id", $dataArray)){

            $this->id = $dataArray['id'];

        }
        if (array_key_exists("name", $dataArray)){

            $this->name = $dataArray['name'];

        }
        if (array_key_exists("profilePicture", $dataArray)){

            $this->profilePicture = $dataArray['profilePicture'];

        }
    } // end of set data


    public function store(){



        $sqlQuery = "INSERT INTO `profile_pictures` (`name`, `profile_picture`) VALUES (?,?); ";

        $dataArray = array ($this->name, $this->profilePicture);

        $sth = $this->dbh->prepare($sqlQuery);

        $results = $sth->execute($dataArray);

        if ($results) {
            Message::message("Success!! << Data inserted successfull >> ");
        } else {
            Message::message("Error: Data has not inserted");

        }

    }// end of store class


    public function index()
    {

        $sqlQuery = "Select * from profile_pictures WHERE is_trashed = 'No'";

        $sth = $this->dbh->query($sqlQuery);

//        Utility::dd($sth);

        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();

        return $allData;
    } // end of index


    public function view(){

        $sqlQuery = " select * from profile_pictures WHERE id =" .$this->id;

    $sth = $this->dbh->query($sqlQuery);
    $sth->setFetchMode(PDO::FETCH_OBJ);

    $singleData = $sth->fetch();
    return $singleData;

    } // end of view


    public function update()
    {

        $sqlQuery = "UPDATE profile_pictures SET name = ?, profile_picture = ? WHERE id = $this->id;";

        $dataArray = array($this->name, $this->profilePicture);

        $sth = $this->dbh->prepare($sqlQuery);
        $result = $sth->execute($dataArray);

        if ($result) {
            Message::message("Success :) Data has been updated successfully.");
        } else {
            Message::message("Failure :( Data has not been updated due to an error.");

        }
    } // end of update

    public function trash (){

$sqlQuery = "UPDATE profile_pictures SET is_trashed = NOW() WHERE id = $this->id;";

$result = $this->dbh->exec($sqlQuery);


        if($result){
            Message::message("Success! Data has been trashed Successfully!");
        }
        else{
            Message::message("Error! Data has not been trashed.");

        }
    }

    public function trashed(){

        $sqlQuery = "Select * from profile_pictures where is_trashed <>'No'";

        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData = $sth->fetchAll();
        return $allData;

    } // end of trashed()


    public function recover(){
        $sqlQuery = "UPDATE profile_pictures SET is_trashed='No' WHERE id = $this->id;";

        $result = $this->dbh->exec($sqlQuery);

        if($result){
            Message::message("Success! Data has been recovered Successfully!");
        }
        else{
            Message::message("Error! Data has not been recovered.");

        }
    } // end of recover method



    public function delete(){
        $sqlQuerry = "DELETE FROM profile_pictures WHERE id = $this->id";

        $result= $this->dbh->exec($sqlQuerry);

        if ($result){
            Message::message("Success! Data has been deleted successfully");
        }else{
            Message::message("Failure! Data has not been deleted due to error");
        }

    } // end of delete method


    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byName']) && isset($requestArray['byPicture']) )  $sql = "SELECT * FROM `profile_pictures` WHERE `is_trashed` ='No' AND (`profile_pictures` LIKE '%".$requestArray['search']."%' OR `name` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byName']) && !isset($requestArray['byPicture']) ) $sql = "SELECT * FROM `profile_pictures` WHERE `is_trashed` ='No' AND `profile_pictures` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byName']) && isset($requestArray['byPicture']) )  $sql = "SELECT * FROM `profile_pictures` WHERE `is_trashed` ='No' AND `name` LIKE '%".$requestArray['search']."%'";

        $sth  = $this->dbh->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $someData = $sth->fetchAll();

        return $someData;

    }// end of search()




    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $someData) {
            $_allKeywords[] = trim($someData->profile_picture);
        }

        $allData = $this->index();


        foreach ($allData as $someData) {

            $eachString= strip_tags($someData->profile_picture);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $someData) {
            $_allKeywords[] = trim($someData->name);
        }
        $allData = $this->index();

        foreach ($allData as $someData) {

            $eachString= strip_tags($someData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords


    public function indexPaginator($page=1,$itemsPerPage=3){


        $start = (($page-1) * $itemsPerPage);

        if($start<0) $start = 0;


        $sqlQuery = "SELECT * from profile_pictures  WHERE is_trashed = 'No' LIMIT $start,$itemsPerPage";


        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $someData  = $sth->fetchAll();

        return $someData;


    } // end of index paginator method

}// end of  Hobbies class