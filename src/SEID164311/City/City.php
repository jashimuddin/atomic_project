<?php


namespace App\City;

use App\Message\Message;
use App\Model\Database;
use App\Utility\Utility;
use PDO;

class City extends Database
{

    public $id;
    public $name;
    public $city;


    public function setData($postArray){

        if(array_key_exists("id",$postArray)){
            $this->id = $postArray['id'];
        }

        if(array_key_exists("name",$postArray)){
            $this->name = $postArray['name'];
        }

        if(array_key_exists("city",$postArray)){
            $this->city = $postArray['city'];
        }

    } // end of setData method

    public function store(){

        $query = "INSERT INTO city (name, city) VALUES (?, ?);";

        $dataArray = array($this->name, $this->city);

        $sth = $this->dbh->prepare($query);
        $result = $sth->execute($dataArray);

        if($result){
            Message::message("Success :) Data Inserted Successfully.");
        }
        else{
            Message::message("Failure :( Data Not Inserted Successfully!");
        }
    } // end of store method


    public function index(){

        $query = "SELECT * FROM city";

        $sth = $this->dbh->query($query);

        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;

    } // end of index method

    public function view(){

        $query = "SELECT * FROM city WHERE id=".$this->id;

        $sth = $this->dbh->query($query);

        $sth->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $sth->fetch();
        return $singleData;

    } // end of view method

    public function update(){

        $query = "UPDATE city SET name = ?,city = ? WHERE id = $this->id;";

        //Utility::dd($query);

        $dataArray = array($this->name, $this->city);

        $sth = $this->dbh->prepare($query);
        $result = $sth->execute($dataArray);

        if($result){
            Message::message("Success :) Data Updated Successfully.");
        }
        else{
            Message::message("Failure :( Data Not Updated!");
        }
    } // end of update method

    public function trash(){
        $sqlQuerry = "UPDATE city SET is_trashed = NOW() WHERE id = $this->id;";
        $result = $this->dbh->exec($sqlQuerry);
        if ($result){
            Message::message("Success! Data has been trashed successfully");
        }else{
            Message::message("Failure! Data has not been trashed due to error");
        }
    }  // end of trash method

    public function trashed(){

        $sqlQuery = "select * from city WHERE is_trashed <> 'No' ";

        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        return $sth->fetchAll();

    } // end of trashed method


    public function recover(){
        $sqlQuery = "UPDATE city SET is_trashed = 'No' WHERE id = $this->id;";
        $result = $this->dbh->exec($sqlQuery);

        if($result){
            Message::message("Success! Data has been recovered successfully!");
        }
        else{
            Message::message("Failure! Data has not been recovered due to an error!");
        }
    } //end of recover()

    public function delete(){
        $sqlQuerry = "DELETE FROM city WHERE id = $this->id";

        $result= $this->dbh->exec($sqlQuerry);

        if ($result){
            Message::message("Success! Data has been deleted successfully");
        }else{
            Message::message("Failure! Data has not been deleted due to error");
        }

    } // end of delete method


    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byName']) && isset($requestArray['byCity']) )  $sql = "SELECT * FROM `city` WHERE `is_trashed` ='No' AND (`city` LIKE '%".$requestArray['search']."%' OR `name` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byName']) && !isset($requestArray['byCity']) ) $sql = "SELECT * FROM `city` WHERE `is_trashed` ='No' AND `city` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byName']) && isset($requestArray['byCity']) )  $sql = "SELECT * FROM `city` WHERE `is_trashed` ='No' AND `name` LIKE '%".$requestArray['search']."%'";

        $sth  = $this->dbh->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $someData = $sth->fetchAll();

        return $someData;

    } // end of search method




    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->city);
        }

        $allData = $this->index();


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->city);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        } // for each search field block end




        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->city);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    } // get all keywords


    public function indexPaginator($page=1,$itemsPerPage=3){


        $start = (($page-1) * $itemsPerPage);

        if($start<0) $start = 0;


        $sqlQuery = "SELECT * from city  WHERE is_trashed = 'No' LIMIT $start,$itemsPerPage";


        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData  = $sth->fetchAll();

        return $allData;


    } // end of index paginator method


}