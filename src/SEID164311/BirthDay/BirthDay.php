<?php

namespace App\BirthDay;

    use App\Message\Message;
    use App\Model\Database;
    use App\Utility\Utility;
    use PDO;

    class BirthDay extends Database {

        public $id;
        public $name;
        public $dob;


        public function setData($postArray){

            if(array_key_exists('id', $postArray)){
                $this->id = $postArray['id'];
            }
            if(array_key_exists('name', $postArray)){
                $this->name = $postArray['name'];
            }

            if(array_key_exists('dob', $postArray)){
                $this->dob = $postArray['dob'];
            }

        } // end of set data


        public function store(){


            $name =$this->name;
            $dob = $this->dob;

            $sqlQuery = "INSERT INTO `birthday` (`name`, `birthdate`) VALUES (?,?); ";

            $dataArray = array($name, $dob);

            $sth = $this->dbh->prepare($sqlQuery);

            $result = $sth->execute($dataArray);  // ey line tay confused

                if ($result){
                    Message::message("Data inserted Successfully");

                } else {
                    Message::message(" Error: Data has not inserted ");
                }
        } // End of store method

        public function index(){

            $sqlQuery = "select * from birthday WHERE is_trashed = 'No' ";
            $sth = $this->dbh->query($sqlQuery);
            $sth->setFetchMode(PDO::FETCH_OBJ);
            $allData = $sth->fetchAll();
            return $allData;  // Correction done !


        }  // end of index


        public function view(){

            $sqlQuery = "select * from birthday WHERE id= " .$this->id;
            $sth = $this->dbh->query($sqlQuery);

            $sth->setFetchMode(PDO::FETCH_OBJ);
            $singleData = $sth->fetch();

            return $singleData;
        } // end of view method

        public function update(){

            $name =$this->name;
            $dob = $this->dob;

            $sqlQuery = "UPDATE birthday SET name =?, birthdate =? WHERE id = $this->id;";

            $dataArray = array($name, $dob);

            $sth = $this->dbh->prepare($sqlQuery);

            $result = $sth->execute($dataArray);  // ey line tay confused

            if ($result){
                Message::message("Data update Successfully");

            } else {
                Message::message(" Error: Data has not updated ");
            }
        } // End of store method

        // trash method started
        public function trash(){
            $sqlQuery ="update birthday SET is_trashed = NOW() where id = $this->id;";

            $result = $this->dbh->exec($sqlQuery);

            if ($result){
                Message::message("Success! Data has been trashed successfully");
            }else{
                Message::message("Failure! Data has not been trashed due to error");
            }
        } // End of trash Method

        public function trashed(){

            $sqlQuery = "select * from birthday WHERE is_trashed <> 'No' ";

            $sth = $this->dbh->query($sqlQuery);

            $sth->setFetchMode(PDO::FETCH_OBJ);

            return $sth->fetchAll();

        } // end of trashed method


        public function recover(){
            $sqlQuery = "UPDATE birthday SET is_trashed = 'No' WHERE id = $this->id;";
            $result = $this->dbh->exec($sqlQuery);

            if($result){
                Message::message("Success! Data has been recovered successfully!");
            }
            else{
                Message::message("Failure! Data has not been recovered due to an error!");
            }
        }



        public function delete(){
            $sqlQuery = " DELETE FROM birthday WHERE id = $this->id" ;
            $result = $this->dbh->exec($sqlQuery);

            if ($result){
                Message::message("Success! Data has been deleted successfully");
            }else{
                Message::message("Failure! Data has not been deleted due to error");
            }
        } // end of delete

        public function search($requestArray){
            $sql = "";
            if( isset($requestArray['byName']) && isset($requestArray['byBirthdate']) )  $sql = "SELECT * FROM `birthday` WHERE `is_trashed` ='No' AND (`name` LIKE '%".$requestArray['search']."%' OR `birthdate` LIKE '%".$requestArray['search']."%')";
            if(isset($requestArray['byName']) && !isset($requestArray['byBirthdate']) ) $sql = "SELECT * FROM `birthday` WHERE `is_trashed` ='No' AND `name` LIKE '%".$requestArray['search']."%'";
            if(!isset($requestArray['byName']) && isset($requestArray['byBirthdate']) )  $sql = "SELECT * FROM `birthday` WHERE `is_trashed` ='No' AND `birthdate` LIKE '%".$requestArray['search']."%'";

            $sth  = $this->dbh->query($sql);
            $sth->setFetchMode(PDO::FETCH_OBJ);
            $allData = $sth->fetchAll();

            return $allData;

        }// end of search()


        public function getAllKeywords()
        {
            $_allKeywords = array();
            $WordsArr = array();

            $allData = $this->index();

            foreach ($allData as $oneData) {
                $_allKeywords[] = trim($oneData->name);
            }

            $allData = $this->index();

            foreach ($allData as $oneData) {

                $eachString= strip_tags($oneData->name);
                $eachString=trim( $eachString);
                $eachString= preg_replace( "/\r|\n/", " ", $eachString);
                $eachString= str_replace("&nbsp;","",  $eachString);

                $WordsArr = explode(" ", $eachString);

                foreach ($WordsArr as $eachWord){
                    $_allKeywords[] = trim($eachWord);
                }
            }
            // for each search field block end




            // for each search field block start
            $allData = $this->index();

            foreach ($allData as $oneData) {
                $_allKeywords[] = trim($oneData->birthdate);
            }
            $allData = $this->index();

            foreach ($allData as $oneData) {

                $eachString= strip_tags($oneData->birthdate);
                $eachString=trim( $eachString);
                $eachString= preg_replace( "/\r|\n/", " ", $eachString);
                $eachString= str_replace("&nbsp;","",  $eachString);
                $WordsArr = explode(" ", $eachString);

                foreach ($WordsArr as $eachWord){
                    $_allKeywords[] = trim($eachWord);
                }
            }
            // for each search field block end


            return array_unique($_allKeywords);


        }//  end of get all keywords


        public function indexPaginator($page=1,$itemsPerPage=3){


            $start = (($page-1) * $itemsPerPage);

            if($start<0) $start = 0;


            $sqlQuery = "SELECT * from birthday  WHERE is_trashed = 'No' LIMIT $start,$itemsPerPage";


            $sth = $this->dbh->query($sqlQuery);

            $sth->setFetchMode(PDO::FETCH_OBJ);

            $allData  = $sth->fetchAll();

            return $allData;


        } // end of index paginator method





    }