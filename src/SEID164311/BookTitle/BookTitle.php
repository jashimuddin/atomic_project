<?php


namespace App\BookTitle;

use App\Message\Message;
use App\Model\Database;
use App\Utility\Utility;
use PDO;

class BookTitle extends Database
{
    public $id;
    public $bookTitle;
    public $authorName;


    public function setData($postArray){

        if (array_key_exists("id",$postArray)){
            $this->id = $postArray['id'];
        }
        if (array_key_exists("bookTitle",$postArray)){
            $this->bookTitle = $postArray['bookTitle'];
        }
        if (array_key_exists("authorName",$postArray)){
            $this->authorName = $postArray['authorName'];
        }
    } //end of set data


    public function store(){

            $bookTitle = $this->bookTitle;
            $authorName = $this->authorName;

            $sqlQuery = "INSERT INTO `book_title` (`book_title`, `author_name`) VALUES (?,?);";

            $dataArray = array($bookTitle, $authorName);

            $sth = $this->dbh->prepare($sqlQuery);

            $result = $sth->execute($dataArray);

            if ($result){
                Message::message("Success! Data has been inserted successfully");
            }else{
                Message::message("Failure! Data has not been inserted due to error");
            }

    }


    /**
     * @return array
     */
    public function index(){

        $sqlQuery = "select * from book_title WHERE is_trashed = 'No' ";
        $sth = $this->dbh->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;  // Correction done !


    }  // end of index

    /**
     * @return mixed
     */
    public function view(){

        $sqlQuery = "select * from book_title WHERE id=" .$this->id;


        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $sth->fetch();
        return $singleData;  // correction done !!


    } // end of view method

    /**
     *
     */
    public function update(){
        $sqlQuerry = "UPDATE book_title SET book_title = ?, author_name = ? WHERE id = $this->id;";

        $dataArray = array($this->bookTitle, $this->authorName);

        $sth = $this->dbh->prepare($sqlQuerry);

        $result = $sth->execute($dataArray);

        if ($result){
            Message::message("Success! Data has been updated successfully");
        }else{
            Message::message("Failure! Data has not been updated due to error");
        }

    }  // end of update method



    public function trash(){
        $sqlQuerry = "UPDATE book_title SET is_trashed = NOW() WHERE id = $this->id;";
        $result = $this->dbh->exec($sqlQuerry);
        if ($result){
            Message::message("Success! Data has been trashed successfully");
        }else{
            Message::message("Failure! Data has not been trashed due to error");
        }
    }  // end of trash method

    public function trashed(){

        $sqlQuery = "select * from book_title WHERE is_trashed <> 'No' ";

        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        return $sth->fetchAll();

    } // end of trashed method


    public function recover(){
        $sqlQuery = "UPDATE book_title SET is_trashed = 'No' WHERE id = $this->id;";
        $result = $this->dbh->exec($sqlQuery);

        if($result){
            Message::message("Success! Data has been recovered successfully!");
        }
        else{
            Message::message("Failure! Data has not been recovered due to an error!");
        }
    }//end of recover()


    public function delete(){
        $sqlQuerry = "DELETE FROM book_title WHERE id = $this->id";

        $result= $this->dbh->exec($sqlQuerry);

            if ($result){
                Message::message("Success! Data has been deleted successfully");
            }else{
                Message::message("Failure! Data has not been deleted due to error");
            }

    } // end of delete method


    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `book_title` WHERE `is_trashed` ='No' AND (`book_title` LIKE '%".$requestArray['search']."%' OR `author_name` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byTitle']) && !isset($requestArray['byAuthor']) ) $sql = "SELECT * FROM `book_title` WHERE `is_trashed` ='No' AND `book_title` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `book_title` WHERE `is_trashed` ='No' AND `author_name` LIKE '%".$requestArray['search']."%'";

        $sth  = $this->dbh->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $someData = $sth->fetchAll();

        return $someData;

    }// end of search()



    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->book_title);
        }

        $allData = $this->index();


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->book_title);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        } // for each search field block end


        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->author_name);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->author_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }   // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords


    public function indexPaginator($page=1,$itemsPerPage=3){


        $start = (($page-1) * $itemsPerPage);

        if($start<0) $start = 0;


        $sqlQuery = "SELECT * from book_title  WHERE is_trashed = 'No' LIMIT $start,$itemsPerPage";


        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $someData  = $sth->fetchAll();

        return $someData;


    } // end of index paginator method


} //end of BookTitle Class