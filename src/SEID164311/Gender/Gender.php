<?php

namespace App\Gender;


    use App\Message\Message;
    use App\Model\Database;
    use App\Utility\Utility;
    use PDO;

    class Gender extends Database
    {
        public $id;
        public $name;
        public $gender;


        public function setData($postArray){
            if (array_key_exists('id', $postArray)){
                $this->id = $postArray['id'];
            }
            if (array_key_exists('name', $postArray)){
                $this->name = $postArray['name'];
            }

            if (array_key_exists('gender', $postArray)){
                $this->gender = $postArray['gender'];
            }


        } // end of set data

        public function store(){


            $sqlQuery = " INSERT INTO gender ( name, gender) VALUES (?,?);";

            $dataArray = array($this->name,$this->gender);

            $sth = $this->dbh->prepare($sqlQuery);
            $result = $sth->execute($dataArray);

            if ($result){
                Message::message("Data inserted Succefully");
            } else{
                Message::message("Data has not been inserted");
            }

        } // end of store data

        public function index()
        {
           $sqlQuery = "select * from gender WHERE is_trashed = 'No' ";

//           Utility::dd($sqlQuery);

            $sth = $this->dbh->query($sqlQuery);
            $sth->setFetchMode(PDO::FETCH_OBJ);
            $singleData = $sth->fetchAll();
            return $singleData;


        } // end of  index method

        public function view(){

            $sqlQuery = "SELECT * FROM gender WHERE id=".$this->id;

//            Utility::dd($sqlQuery);
            $sth = $this->dbh->query($sqlQuery);

            $sth->setFetchMode(PDO::FETCH_OBJ);
            $singleData = $sth->fetch();

            return $singleData;
        } //end of view method


        public function update(){
            $sqlQuerry = "UPDATE gender SET name = ?, gender = ? WHERE id = $this->id;";

            $dataArray = array($this->name, $this->gender);

            $sth = $this->dbh->prepare($sqlQuerry);

            $result = $sth->execute($dataArray);

            if ($result){
                Message::message("Success! Data has been updated successfully");
            }else{
                Message::message("Failure! Data has not been updated due to error");
            }

        } // end of update method

        public function trash(){
            $sqlQuerry = "UPDATE gender SET is_trashed = NOW() WHERE id = $this->id;";
            $result = $this->dbh->exec($sqlQuerry);
            if ($result){
                Message::message("Success! Data has been trashed successfully");
            }else{
                Message::message("Failure! Data has not been trashed due to error");
            }
        }  // end of trash method

        public function trashed(){

            $sqlQuery = "select * from gender WHERE is_trashed <> 'No' ";

            $sth = $this->dbh->query($sqlQuery);

            $sth->setFetchMode(PDO::FETCH_OBJ);

            return $sth->fetchAll();

        } // end of trashed method


        public function recover(){
            $sqlQuery = "UPDATE gender SET is_trashed = 'No' WHERE id = $this->id;";
            $result = $this->dbh->exec($sqlQuery);

            if($result){
                Message::message("Success! Data has been recovered successfully!");
            }
            else{
                Message::message("Failure! Data has not been recovered due to an error!");
            }
        } //end of recover()

        public function delete(){
            $sqlQuerry = "DELETE FROM gender WHERE id = $this->id";

            $result= $this->dbh->exec($sqlQuerry);

            if ($result){
                Message::message("Success! Data has been deleted successfully");
            }else{
                Message::message("Failure! Data has not been deleted due to error");
            }

        } // end of delete method


        public function search($requestArray){
            $sql = "";
            if( isset($requestArray['byName']) && isset($requestArray['byGender']) )  $sql = "SELECT * FROM `gender` WHERE `is_trashed` ='No' AND (`gender` LIKE '".$requestArray['search']."' OR `name` LIKE '".$requestArray['search']."')";
            if(isset($requestArray['byName']) && !isset($requestArray['byGender']) ) $sql = "SELECT * FROM `gender` WHERE `is_trashed` ='No' AND `gender` LIKE '".$requestArray['search']."'";
            if(!isset($requestArray['byName']) && isset($requestArray['byGender']) )  $sql = "SELECT * FROM `gender` WHERE `is_trashed` ='No' AND `name` LIKE '".$requestArray['search']."'";

            $sth  = $this->dbh->query($sql);
            $sth->setFetchMode(PDO::FETCH_OBJ);
            $someData = $sth->fetchAll();

            return $someData;

        } // end of search()




        public function getAllKeywords()
        {
            $_allKeywords = array();
            $WordsArr = array();

            $allData = $this->index();

            foreach ($allData as $someData) {
                $_allKeywords[] = trim($someData->gender);  // changing for test
            }

            $allData = $this->index();


            foreach ($allData as $someData) {

                $eachString= strip_tags($someData->gender);
                $eachString=trim( $eachString);
                $eachString= preg_replace( "/\r|\n/", " ", $eachString);
                $eachString= str_replace("&nbsp;","",  $eachString);

                $WordsArr = explode(" ", $eachString);

                foreach ($WordsArr as $eachWord){
                    $_allKeywords[] = trim($eachWord);
                }
            } // for each search field block end




            // for each search field block start
            $allData = $this->index();

            foreach ($allData as $someData) {
                $_allKeywords[] = trim($someData->name);
            }
            $allData = $this->index();

            foreach ($allData as $someData) {

                $eachString= strip_tags($someData->gender);
                $eachString=trim( $eachString);
                $eachString= preg_replace( "/\r|\n/", " ", $eachString);
                $eachString= str_replace("&nbsp;","",  $eachString);
                $WordsArr = explode(" ", $eachString);

                foreach ($WordsArr as $eachWord){
                    $_allKeywords[] = trim($eachWord);
                }
            }
            // for each search field block end


            return array_unique($_allKeywords);


        } // get all keywords


        public function indexPaginator($page=1,$itemsPerPage=3){


            $start = (($page-1) * $itemsPerPage);

            if($start<0) $start = 0;


            $sqlQuery = "SELECT * from gender  WHERE is_trashed = 'No' LIMIT $start,$itemsPerPage";


            $sth = $this->dbh->query($sqlQuery);

            $sth->setFetchMode(PDO::FETCH_OBJ);

            $someData  = $sth->fetchAll();

            return $someData;


        } // end of index paginator method
        
        
    } // end of Gender class